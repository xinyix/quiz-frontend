import React, {Component} from 'react';
import {addGoods} from "../actions/addGoodsAction";
import {connect} from "react-redux";
import '../styles/addGoods.less'

class AddGoods extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      name: '',
      price: 0,
      unit: '',
      picturelink: ''
    };
    this.nameChange = this.nameChange.bind(this);
    this.priceChange = this.priceChange.bind(this);
    this.unitChange = this.unitChange.bind(this);
    this.picturelinkChange = this.picturelinkChange.bind(this);
    this.submit = this.submit.bind(this);
  }

  nameChange(event) {
    this.setState({
      name: event.target.value
    });
  }

  priceChange(event) {
    this.setState({
      price: event.target.value
    });
  }

  unitChange(event) {
    this.setState({
      unit: event.target.value
    });
  }

  picturelinkChange(event) {
    this.setState({
      picturelink: event.target.value
    });
  }

  submit() {
    this.props.addGoods(this.state.name, this.state.price, this.state.unit, this.state.picturelink);
  }

  render() {
    return (
      <section className='AddGoods'>
        <h1>添加商品</h1>
        <section>
          <h3><span>*</span>名称：</h3>
          <input type="text" placeholder='名称' onChange={this.nameChange} className='input'/>
          <h3><span>*</span>价格：</h3>
          <input type="text" placeholder='价格' onChange={this.priceChange} className='input'/>
          <h3><span>*</span>单位：</h3>
          <input type="text" placeholder='单位' onChange={this.unitChange} className='input'/>
          <h3><span>*</span>图片：</h3>
          <input type="text" placeholder='图片' onChange={this.picturelinkChange} className='input'/>
        </section>
        <button onClick={this.submit}>提交</button>
      </section>
    );
  }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  addGoods: (name, price, unit, picturelink) => dispatch(addGoods(name, price, unit, picturelink))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddGoods);