import {getGoods} from "../../ShoppingMall/actions/getGoodsAction";

export const addGoods = (name, price, unit, picturelink) => (dispatch => {
  const goodsInfo = {
    name: name,
    price: price,
    unit: unit,
    picturelink: picturelink
  };

  const requestParams = {
    body: JSON.stringify(goodsInfo),
    headers: {
      'content-type': 'application/json; charset=UTF-8'
    },
    method: 'POST'
  };

  fetch('http://localhost:8080/api/goods', requestParams)
    .then(response => {
      if (response.status === 400) {
        response.text().then(value => alert(value));
      }
      dispatch(getGoods())
    })
    .catch(error => alert(error));
});