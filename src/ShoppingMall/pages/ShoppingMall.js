import React, {Component} from 'react';
import {getGoods} from "../actions/getGoodsAction";
import {connect} from "react-redux";
import {addOrder} from "../../Orders/actions/addOrderAction";
import '../styles/shoppingMall.less'

class ShoppingMall extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      disabled: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(name) {
    this.setState({
      disabled: true
    });
    this.props.addOrder(name, 1);
    this.setState({
      disabled: false
    });
  }

  componentDidMount() {
    this.props.getGoods();
  }

  render() {
    return (
      <section className='shoppingMall'>
        <ul>
          {this.props.goods.map(good => {
            return (
              <li key={good.id} className='goodsList'>
                <section>
                  <img src={good.picturelink} alt={good.name} width="100" height="100"/>
                  <h3>{good.name}</h3>
                  <p>单价：{good.price}元/{good.unit}</p>
                  <button onClick={() => this.handleClick(good.name)} disabled={this.state.disabled}>+</button>
                </section>
              </li>)
          })}
        </ul>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  goods: state.getGoodsReducer.goods,
  orders: state.getOrdersReducer.orders,
});

const mapDispatchToProps = (dispatch) => ({
  getGoods: () => dispatch(getGoods()),
  addOrder: (name, quantity) => dispatch(addOrder(name, quantity))
});

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingMall);