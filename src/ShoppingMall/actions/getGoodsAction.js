export const GET_GOODS_TYPE = 'GET_GOODS';

export const getGoods = () => ((dispatch) => {
  return fetch('http://localhost:8080/api/goods')
    .then(response => response.json())
    .then(data => dispatch({
      type: GET_GOODS_TYPE,
      payload: data
    }))
});