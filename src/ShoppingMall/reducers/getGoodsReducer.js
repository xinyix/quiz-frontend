import {GET_GOODS_TYPE} from "../actions/getGoodsAction";

const initState = {
  goods: []
};

export default function getGoodsReducer(state=initState, action) {
  if (action.type === GET_GOODS_TYPE) {
    return {
      ...state,
      goods: action.payload
    }
  } else {
    return state;
  }
}