import {combineReducers} from "redux";
import getGoodsReducer from "../ShoppingMall/reducers/getGoodsReducer";
import getOrdersReducer from "../Orders/reducers/getOrdersReducer";

const reducers = combineReducers({
  getGoodsReducer: getGoodsReducer,
  getOrdersReducer: getOrdersReducer,
});
export default reducers;