import React, {Component} from 'react';
import './App.less';
import {BrowserRouter, Link} from "react-router-dom";
import {Route, Switch} from "react-router";
import ShoppingMall from "./ShoppingMall/pages/ShoppingMall";
import AddGoods from "./AddGoods/pages/AddGoods";
import Order from "./Orders/pages/Orders";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <BrowserRouter>
          <header>
            <Link to='/'>商城</Link>
            <Link to='/orders'>订单</Link>
            <Link to='/add'>添加商品</Link>
          </header>
          <Switch>
            <Route exact path='/' component={ShoppingMall}/>
            <Route exact path='/add' component={AddGoods}/>
            <Route exact path='/orders' component={Order}/>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;