export const addOrder = (name, quantity) => (dispatch => {
  const orderInfo = {
    name: name,
    quantity: quantity
  };

  const requestParams = {
    body: JSON.stringify(orderInfo),
    headers: {
      'content-type': 'application/json; charset=UTF-8'
    },
    method: 'POST'
  };

  fetch('http://localhost:8080/api/orders', requestParams)
    .then(
      response => response
    )
    .catch(error => alert(error));
});