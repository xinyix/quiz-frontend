export const GET_ORDER_TYPE = 'GET_ORDER';

export const getOrders = () => ((dispatch) => {
  return fetch('http://localhost:8080/api/orders')
    .then(response => response.json())
    .then(data => dispatch({
      type: GET_ORDER_TYPE,
      payload: data
    }))
});