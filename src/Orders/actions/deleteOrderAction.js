import {getOrders} from "./getOrdersAction";

export const deleteOrder = (name) => (dispatch => {
  const requestParams = {
    headers: {
      'content-type': 'application/json; charset=UTF-8'
    },
    method: 'DELETE'
  };

  fetch(`http://localhost:8080/api/orders/${name}`, requestParams)
    .then(
      response => {
        if (response.status === 200) {
          dispatch(getOrders());
        } else {
          alert("订单删除失败，请稍后再试")
        }
      }
    )
    .catch(error => alert(error));
});