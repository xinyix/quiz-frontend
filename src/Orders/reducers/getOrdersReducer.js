import {GET_ORDER_TYPE} from "../actions/getOrdersAction";

const initState = {
  orders: []
};

export default function getOrdersReducer(state=initState, action) {
  if (action.type === GET_ORDER_TYPE) {
    return {
      ...state,
      orders: action.payload
    }
  } else {
    return state;
  }
}