import React, {Component} from 'react';
import {getOrders} from "../actions/getOrdersAction";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {getGoods} from "../../ShoppingMall/actions/getGoodsAction";
import {deleteOrder} from "../actions/deleteOrderAction";
import '../styles/orders.less'

class Orders extends Component {
  componentDidMount() {
    this.props.getOrders();
    if (this.props.goods.length === 0) {
      this.props.getGoods();
    }
  }

  render() {
    return (
      <section className='orders'>
        {this.props.orders.length === 0 ?
          <section>
            <p id='hint'>暂无订单，返回商场页面继续购买</p>
            <Link to='/' id='back-to-shopping-mall'>商城页面</Link>
          </section> :
          <table>
            <tr id='table-head'>
              <td>名字</td>
              <td>单价</td>
              <td>数量</td>
              <td>单位</td>
              <td>操作</td>
            </tr>
            {this.props.orders.map(order => {
              const goods = this.props.goods.find(good => good.name === order.name);
              return (
                <tr key={order.id}>
                  <td>{order.name}</td>
                  <td>{goods === undefined ? null : goods.price}</td>
                  <td>{order.quantity}</td>
                  <td>{goods === undefined ? null : goods.unit}</td>
                  <td><button onClick={() => this.props.deleteOrder(order.name)}>删除</button></td>
                </tr>)
            })}

          </table>}
      </section>
    );
  }
}

const mapStateToProps = state => ({
  goods: state.getGoodsReducer.goods,
  orders: state.getOrdersReducer.orders
});

const mapDispatchToProps = (dispatch) => ({
  getGoods: () => dispatch(getGoods()),
  getOrders: () => dispatch(getOrders()),
  deleteOrder: (name) => dispatch(deleteOrder(name))
});

export default connect(mapStateToProps, mapDispatchToProps)(Orders);